Create table person (
    id serial not null,
    firstname varchar default (30),
    lastname varchar default (30),
    city varchar default (30),
    phone varchar default (30),
    telegram varchar default (30)
);
