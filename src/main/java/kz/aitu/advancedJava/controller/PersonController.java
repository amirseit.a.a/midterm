package kz.aitu.advancedJava.controller;

import kz.aitu.advancedJava.model.Person;
import kz.aitu.advancedJava.repository.PersonRepository;
import kz.aitu.advancedJava.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class PersonController {

    @Autowired
    private final PersonRepository personRepository;

    @Autowired
    private PersonService personService;

    public PersonController(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @GetMapping("/api/persons/{id}")
    public ResponseEntity<?> getRecord(@PathVariable Long id){
        return ResponseEntity.ok(personService.getById(id));
    }

    @GetMapping("/api/v2/users")
   public String showTables(Model model){
        Iterable<Person> elem = personRepository.findAll();
        model.addAttribute("elem",elem);
        return "index";
    }

    @PostMapping("/api/persons")
    public ResponseEntity<?> savePerson(@RequestBody Person person){
        return ResponseEntity.ok(personService.create(person));
    }

    @PutMapping("/api/persons")
    public ResponseEntity<?> updatePerson(@RequestBody Person person){
        return ResponseEntity.ok(personService.update(person));
    }

    @DeleteMapping("/api/persons/{id}")
    public void deletePerson(@PathVariable Long id){
        personService.delete(id);
    }

}
