package kz.aitu.advancedJava;

import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.LogRecord;

@Component
public class CorsFilter implements Filter {
    private ServletRequest req;
    private ServletResponse res;
    private FilterChain chain;

    public void doFilter(ServletRequest req,
                         ServletResponse res,
                         FilterChain chain) throws IOException, ServletException {
        this.req = req;
        this.res = res;
        this.chain = chain;
        HttpServletResponse response = (HttpServletResponse) res;
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers", "*");
        try {
            chain.doFilter(req, res);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.err.println("Aborted by user!");
        }
        System.out.println(((HttpServletRequest)req).getRequestURI());
    }
    public void init(FilterConfig filterConfig) {}
    public void destroy() {}

}
